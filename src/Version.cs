﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyDescription("XTS-AES implementation for .NET")]
[assembly: AssemblyCompany("Gareth Lennox")]
[assembly: AssemblyProduct("XTSSharp")]
[assembly: AssemblyCopyright("Copyright © 2010 Gareth Lennox")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("0.1.1")]
[assembly: AssemblyFileVersion("0.1.1")]